import express from 'express';
import { ApolloServer } from 'apollo-server-express';
import mongoose from 'mongoose';
import bodyParser from 'body-parser'
import { typeDefs } from './typeDefs'
import { resolvers } from './resolvers'
import { Error } from './errors';
import helmet from 'helmet'
import cors from 'cors'
import { User } from './models/User';
import { License } from './models/License';

const startServer = async () => {
    const app = express();
    app.use(bodyParser.json())
    app.use(bodyParser.urlencoded({ extended: true }))
    // app.use(helmet())
    // app.use(cors())
    const server = new ApolloServer({
        typeDefs,
        resolvers,
        context: async ({ req }) => {
            var tok = req.headers.authorization || 'Bearer null';
            var ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
            if (ip == "::1") ip = "127.0.0.1";
            if (ip.match(/::ffff:/i)) {
                ip = ip.replace(/::ffff:/i, "");
            }
            tok = tok.replace(/Bearer /i, '')
            var user = await User.findOne({ token: tok })
            var admin = false
            if (user) {
                let check = await License.findOne({ key: user.key })
                if (check) {
                    if (check.model === 'admin') {
                        admin = check
                    }
                }
                if (parseInt(user.expired) < Date.now()) {
                    user = false
                }
            }
            return { user, ip: ip, admin }
        }
    });

    server.applyMiddleware({ app });

    await mongoose.connect('mongodb://Admur04:Admur04012000@cluster0-shard-00-00.q77zg.mongodb.net:27017,cluster0-shard-00-01.q77zg.mongodb.net:27017,cluster0-shard-00-02.q77zg.mongodb.net:27017/wizcard?ssl=true&replicaSet=atlas-mhx24q-shard-0&authSource=admin&retryWrites=true&w=majority', { useNewUrlParser: true, useUnifiedTopology: true });

    app.use((req, res) => {
        res.json({ code: 401, message: 'Unauthorized' });
        res.end();
    });

    app.use(Error)

    app.listen({ port: 4000 }, () =>
        console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath}`)
    )
}

startServer()