import mongoose from 'mongoose'
let Schema = mongoose.Schema
let productSchema = new Schema({
    key: String,
    email: String,
    ip: String,
    domain: String,
    status: String
})

export const Product = mongoose.model('product', productSchema)